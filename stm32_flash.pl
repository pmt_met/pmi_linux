#!/usr/bin/perl
#NOTE: needs libnet-telnet-perl package.
use Net::Telnet;
 
$numArgs = $ARGV + 1;
if($numArgs != 1) {
    die( "Usage ./stm32_flash.pl [./build/PMI.bin] \n");
}

$file = $ARGV[0];

$ip = "127.0.0.1";
$port = 4444;

$telnet = new Net::Telnet (
    Port   => $port,
    Timeout=> 30,
    Errmode=> 'die',
    Prompt => '/>/');

$telnet->open($ip);

print $telnet->cmd('reset halt');
print $telnet->cmd('flash probe 0');
print $telnet->cmd('flash write_image erase '.$file.' 0x08000000');
print $telnet->cmd('reset');
print $telnet->cmd('exit');

print "\n";

