#ifndef LASER_SCANNER_UNIT_H
#define LASER_SCANNER_UNIT_H

#include "PMI.h"

#define LSU_GPIO_SS_PORT		GPIOA
#define LSU_GPIO_SS_PIN		GPIO_Pin_3

#define LSU_GPIO_LD_PORT		GPIOA
#define LSU_GPIO_LD_PIN		GPIO_Pin_2

#define LSU_GPIO_SCK_PORT	GPIOB
#define LSU_GPIO_SCK_PIN		GPIO_Pin_0
#define LSU_GPIO_SCK_TIMER	TIM3


/*function*/
void LSU_GPIO_Config();
void LSU_RCC_config();
void LSU_PWM_config();
void LSU_Init();

#endif /*LASER_SCANNER_UNIT_H*/

