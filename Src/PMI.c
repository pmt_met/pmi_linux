#include "PMI.h"

FATFS fsa;        /* Work area (file system object) for logical drive */
FIL fsrc;         /* file objects */   
FRESULT res;

int Current_status = -2;
bool on_air = false;
extern volatile uint8_t UART_received_char;
char pre_uart_rx = ' ';
int main(void)
{
     /*Matumba variable*/
    int i = 0;
     /*Manual variable*/
    hcode a;
     /*Init funtion*/
    Extern_funtion_init();
    PWM_Init();
    Init_motor_position();
    UART_INIT();
    printf("\nHellozzzzzzzzzzzzz ");
    //while(UART_received_char != 'g')
    while(1)
    {
        USART_Cmd(SERIAL, ENABLE);
        /*move spindle to right position*/
        UART_SendStr("\n HANDJOB");

        PWM_High_speed();
        PWM_Set_speed(MotorZ_Timer, PERIOD_LOW_FREQ);
        
        while(pre_uart_rx != 'g' && pre_uart_rx != 'h')
        {
            pre_uart_rx = DoYourHandJob();
        }

        /*Matubaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa*/
        UART_SendStr("\n MATUMBA");
        DoYourMatumba();
        
        while(pre_uart_rx != 'g' && pre_uart_rx != 'h')
        {
            pre_uart_rx = DoYourHandJob();
        }
    }
    UART_SendStr("\n Done!!!");
    return 0;
}

uint16_t DoYourHandJob()
{
    char feedback = UART_received_char;
    switch(feedback)
    {
        case 'w':
            PWM_Move(_move_backward);
        break;
        case 'd':

            PWM_Move(_move_forward);
        break;

        case 'a':
            PWM_Move(_move_left);
        break;

        case 'x':
            PWM_Move(_move_right);
        break;

        case 'u':
            PWM_Move(_move_up);
        break;

        case 'j':
            PWM_Move(_move_down);
        break;

        case 'f':
        case 'y':
        break;
        default:
            TIM_Cmd(MotorX_Timer, DISABLE);
            TIM_Cmd(MotorY_Timer, DISABLE);
            TIM_Cmd(MotorZ_Timer, DISABLE);
        break;

    }
    return feedback;
}
void DoYourMatumba()
{
    hcode a;
    bool done = false;
    int i = 0;
    char buffer[50][30];
    static int z_hight = 0;
    f_mount(0,&fsa);

    res = f_open( &fsrc , "0:/hcode.hc" , FA_READ);

    /*******************Wait for start button********************/
    if (pre_uart_rx != 'h')
        waiting_is_happy();

    /**/
    if ( res == FR_OK )
    {
        f_gets(buffer[i],30,&fsrc);
        f_gets(buffer[i],30,&fsrc);
        /**/
        while(!done)
        {

            for(i = 0; i < 50; i++)
            {
                f_gets(buffer[i],30,&fsrc);
            }
            for(i = 0; i < 50; i++)
            {
                hc_init(&a);
                a = hc_string_to_hcode(buffer[i]);

                if(a.action == IN_THE_END)
                {
                    done = true;
                    // break;
                }
                Current_status = a.action;
                if(a.step[step_z] > 0)
                    on_air = true;
                else if(a.step[step_z] < 0)
                    on_air = false;
                if (pre_uart_rx != 'h')
                {    
                    switch(a.action)
                    {
                        case ACT_DRILL:
                            UART_SendStr("\n Drill");
                            Change_tool();
                            break;

                        case ACT_MILL:
                            UART_SendStr("\n Mill");
                            Change_tool();
                            break;

                        case ACT_CUT:
                            UART_SendStr("\n Cut");
                            Change_tool();
                            break;

                        case ACT_SIDE:
                            UART_SendStr("\n Flip");
                            Change_tool();
                            break;

                        case ACT_Z_LEVELING:
                            hc_init_vl(&a,0,0,0,0);
                            PWM_Low_speed();
                            Auto_z_leveling_Point(a,false);

                            waiting_is_happy();
                            break;

                        /*I'm a simple switch,
                        I see those command and i just breakkkkkkkkkkk */
                        case ACT_NORMAL:	break;
                        case ACT_FASTMOVE:	break;
                        case ACT_STOP:		break;
                        default: 			break;
                    }
                }
                /*Just do this*/
                if(on_air)
                    PWM_High_speed();
                else
                {
                    PWM_High_speed();
                    PWM_Set_speed(MotorZ_Timer,PERIOD_LOW_FREQ);
                }
                PWM_Enforce_Hcode(a);
                while(!left_step_equal_zeros());
            }
            i = 0;
        }
    }
    else
        UART_SendNumb(res);
  
    pre_uart_rx = ' '; 
    f_close(&fsrc);  
}
