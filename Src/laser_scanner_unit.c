#include "laser_scanner_unit.h"

void LSU_GPIO_Config()
{
	GPIO_InitTypeDef gpio;
	
	gpio.GPIO_Mode = GPIO_Mode_IPU;
	gpio.GPIO_Pin = LSU_GPIO_LD_PIN;
	GPIO_Init(LSU_GPIO_LD_PORT, &gpio);
	
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = LSU_GPIO_SCK_PIN;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LSU_GPIO_SCK_PORT, &gpio);
	
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin =  LSU_GPIO_SS_PIN;
	GPIO_Init(LSU_GPIO_SS_PORT, &gpio);
	
}
void LSU_RCC_config()
{
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);
     RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE);
}
void LSU_PWM_config()
{
	uint16_t Prescaler  = 71; 
     uint16_t PWM_Freq   = 10000; 
     uint16_t Period     = 100;
     uint16_t Pulse      = Period >> 1;
     
     TIM_TimeBaseInitTypeDef	 	 	motor_timer;
     TIM_OCInitTypeDef 				motor_pwm;
     
     PWM_RCC_Init();
     PWM_GPIO_Init();
     
     motor_timer.TIM_ClockDivision  		= 0;
     motor_timer.TIM_CounterMode 		     = TIM_CounterMode_Up;
     motor_timer.TIM_Prescaler 			= Prescaler;
     motor_timer.TIM_Period 				= Period;
     motor_timer.TIM_RepetitionCounter       = 0;

     TIM_TimeBaseInit(LSU_GPIO_SCK_TIMER, &motor_timer);
     
     motor_pwm.TIM_OCMode = TIM_OCMode_PWM1;
     motor_pwm.TIM_OutputState = TIM_OutputState_Enable;
     motor_pwm.TIM_OutputNState = TIM_OutputNState_Enable;
     motor_pwm.TIM_Pulse = Pulse;
     motor_pwm.TIM_OCPolarity = TIM_OCPolarity_Low;
     motor_pwm.TIM_OCNPolarity = TIM_OCNPolarity_High;
     motor_pwm.TIM_OCIdleState = TIM_OCIdleState_Set;
     motor_pwm.TIM_OCNIdleState = TIM_OCIdleState_Reset;

     TIM_OC3Init(MotorX_Timer, &motor_pwm);
          
     TIM_CtrlPWMOutputs(MotorX_Timer, ENABLE);  
	
	TIM_Cmd(LSU_GPIO_SCK_TIMER, ENABLE);
}
void LSU_Init()
{
	LSU_RCC_config();
	LSU_GPIO_Config();
	LSU_PWM_config();
}