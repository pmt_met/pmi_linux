/* Define to prevent recursive inclusion*/
#ifndef __HPDRAGON_INIT_H
#define __HPDRAGON_H

#include "stm32f10x_rcc.h"

/*
 * Configure the clocks, GPIO and other peripherals as required by the demo.
 */
void INIT_prvSetupHardware( void );

#endif /* __HPDRAGON_INIT_H */
